import 'package:cart_geek/api/api_base_helper.dart';
import 'package:cart_geek/model/DashboardModel.dart';

class DashboardRepository {

  ApiBaseHelper _helper = ApiBaseHelper();

  Future<DashboardModel> fetchMovieList() async {
    final response = await _helper.get("/car_geek/dashboard");
    return DashboardModel.fromJson(response);
  }
}
