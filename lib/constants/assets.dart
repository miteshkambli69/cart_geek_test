class Assets {
  Assets._();

  // splash screen assets
  static const String appLogo = "assets/images/ic_app_logo.png";
  static const String pin = "assets/images/ic_pin.png";


  // login screen assets
  static const String carBackground = "assets/images/ic_app_background.png";
  static const String dashboard = "assets/images/ic_dashboard.png";

}