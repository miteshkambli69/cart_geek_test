import 'package:cart_geek/model/From.dart';

class CurrentBookings {
  String packageTitle;
  From from;
  From to;

  CurrentBookings({this.packageTitle, this.from, this.to});

  CurrentBookings.fromJson(Map<String, dynamic> json) {
    packageTitle = json['package_title'];
    from = json['from'] != null ? new From.fromJson(json['from']) : null;
    to = json['to'] != null ? new From.fromJson(json['to']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['package_title'] = this.packageTitle;
    if (this.from != null) {
      data['from'] = this.from.toJson();
    }
    if (this.to != null) {
      data['to'] = this.to.toJson();
    }
    return data;
  }
}