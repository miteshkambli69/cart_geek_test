import 'AllPackages .dart';
import 'CurrentBookings .dart';

class DashboardModel {
  String name;
  String bannerTitle;
  CurrentBookings currentBookings;
  List<AllPackages> allPackages;

  DashboardModel(
      {this.name, this.bannerTitle, this.currentBookings, this.allPackages});

  DashboardModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    bannerTitle = json['banner_title'];
    currentBookings = json['current_bookings'] != null
        ? new CurrentBookings.fromJson(json['current_bookings'])
        : null;
    if (json['all_packages'] != null) {
      allPackages = new List<AllPackages>();
      json['all_packages'].forEach((v) {
        allPackages.add(new AllPackages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['banner_title'] = this.bannerTitle;
    if (this.currentBookings != null) {
      data['current_bookings'] = this.currentBookings.toJson();
    }
    if (this.allPackages != null) {
      data['all_packages'] = this.allPackages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}