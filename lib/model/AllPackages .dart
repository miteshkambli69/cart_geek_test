class AllPackages {
  String packageName;
  String packageDescription;
  String packagePrice;

  AllPackages({this.packageName, this.packageDescription, this.packagePrice});

  AllPackages.fromJson(Map<String, dynamic> json) {
    packageName = json['package_name'];
    packageDescription = json['package_description'];
    packagePrice = json['package_price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['package_name'] = this.packageName;
    data['package_description'] = this.packageDescription;
    data['package_price'] = this.packagePrice;
    return data;
  }
}