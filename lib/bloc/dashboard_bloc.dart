import 'dart:async';

import 'package:cart_geek/api/api_response.dart';
import 'package:cart_geek/model/DashboardModel.dart';
import 'package:cart_geek/repository/dashboard_repository.dart';

class DashboardBloc {
  DashboardRepository _dashboardRepository;

  StreamController _dashboardListController;

  StreamSink<ApiResponse<DashboardModel>> get movieListSink =>
      _dashboardListController.sink;

  Stream<ApiResponse<DashboardModel>> get dashboardListStream =>
      _dashboardListController.stream;

  DashboardBloc() {
    _dashboardListController = StreamController<ApiResponse<DashboardModel>>();
    _dashboardRepository = DashboardRepository();
    fetchDashBoardList();
  }

  fetchDashBoardList() async {
    movieListSink.add(ApiResponse.loading('Fetching data'));
    try {
      DashboardModel movies = await _dashboardRepository.fetchMovieList();
      movieListSink.add(ApiResponse.completed(movies));
    } catch (e) {
      movieListSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _dashboardListController?.close();
  }
}
