import 'dart:ui';

import 'package:cart_geek/constants/assets.dart';
import 'package:cart_geek/screens/dashboard/dashboard.dart';
import 'package:cart_geek/screens/signup/signup.dart';
import 'package:cart_geek/widgets/app_icon_widget.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _controller = TextEditingController();
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: SafeArea(
            child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/ic_app_background.png"),
                      fit: BoxFit.fill),
                ),
                padding: EdgeInsets.all(10),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: size.height * 0.05),
                      Center(child: AppIconWidget(image: Assets.appLogo)),
                      SizedBox(height: size.height * 0.03),
                      Text(
                        "Sign in to continue",
                        style: TextStyle(
                          fontFamily: 'Alegreya Sans',
                          fontSize: 25,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(38, 47, 113, 0.8),
                        ),
                      ),
                      SizedBox(height: size.height * 0.03),
                      new Stack(
                        children: <Widget>[
                          Container(
                            decoration: new BoxDecoration(color: Colors.white),
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(top: 5.0),
                            child: new Card(
                              elevation: 3.0,
                              semanticContainer: true,
                              margin: EdgeInsets.all(30),
                              child: new Column(
                                children: <Widget>[
                                  SizedBox(height: size.height * 0.05),
                                  Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: new Column(
                                      children: <Widget>[
                                        TextField(
                                          keyboardType: TextInputType.number,
                                          maxLength: 10,
                                          controller: _controller,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  204, 204, 204, 1.0),
                                              fontFamily: 'Alegreya Sans',
                                              fontSize: 18,
                                              fontWeight: FontWeight.w300),
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromRGBO(
                                                      227, 109, 166, 1.0)),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromRGBO(
                                                      227, 109, 166, 1.0)),
                                            ),
                                            hintText: "Mobile Number",
                                            suffixIcon: IconButton(
                                              onPressed: () =>
                                                  _controller.clear(),
                                              icon: ImageIcon(
                                                AssetImage(
                                                    "assets/images/ic_mobile.png"),
                                                color: Color.fromRGBO(
                                                    227, 109, 166, 1.0),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: size.height * 0.05),
                                        RaisedButton(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 15, horizontal: 60),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25.0),
                                          ),
                                          onPressed: () {
                                            validateMobileNo(
                                                context, _controller.text);
                                          },
                                          color: Color.fromRGBO(
                                              227, 109, 166, 1.0),
                                          textColor: Colors.white,
                                          child: Text("Verify",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: 'Alegreya Sans',
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        SizedBox(height: size.height * 0.02),
                                        new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              "Don’t have an Account ? ",
                                              style: TextStyle(
                                                fontFamily: 'NexaDemo',
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300,
                                                color: Color.fromRGBO(
                                                    38, 47, 113, 0.8),
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: (){
                                                navigateToSignUp(context);
                                              },
                                              child: Text(
                                                "Sign Up",
                                                style: TextStyle(
                                                  fontFamily: 'NexaDemo',
                                                  fontSize: 16,
                                                  color: Color.fromRGBO(
                                                      38, 47, 113, 0.8),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        SizedBox(height: size.height * 0.01),
                                        GestureDetector(
                                          child: Text(
                                            "Forgot Password?",
                                            style: TextStyle(
                                              fontFamily: 'NexaDemo',
                                              fontSize: 12,
                                              color: Color.fromRGBO(
                                                  227, 109, 166, 1.0),
                                              fontWeight: FontWeight.w300,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: size.height * 0.01),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Image.asset(
                              Assets.pin,
                              width: 90,
                              height: 111,
                            ),
                          )
                        ],
                      ),
                      GestureDetector(
                        child: Text(
                          "Skip",
                          style: TextStyle(
                            fontFamily: 'Alegreya San',
                            fontSize: 15,
                            color: Color.fromRGBO(38, 47, 113, 0.8),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    ],
                  ),
                ))));
  }

  validateMobileNo(context, String value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Please enter mobile number'),
          backgroundColor: Colors.red));
    } else if (!regExp.hasMatch(value)) {
      return Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Please enter valid mobile number'),
        backgroundColor: Colors.red,
      ));
    }else{
      FocusScope.of(context).requestFocus(FocusNode());
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => DashboardScreen()));
    }
    return null;

  }
  navigateToSignUp(context){
    FocusScope.of(context).requestFocus(FocusNode());
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SignUpScreen()));
  }
  }

