import 'dart:ui';

import 'package:cart_geek/constants/assets.dart';
import 'package:cart_geek/screens/dashboard/dashboard.dart';
import 'package:cart_geek/screens/login/login.dart';
import 'package:cart_geek/widgets/app_icon_widget.dart';
import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _Body createState() => new _Body();
}

class _Body extends State<Body> {
  int _value = 1;
  int _second_value = 1;

  // Default Radio Button Selected Item When App Starts.
  String radioButtonItem = 'ONE';
  // Group Value for Radio Button.
  int id = 1;
  int number = 1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _parentNameController = TextEditingController();
    var _mobileNoController = TextEditingController();
    var _emailIdController = TextEditingController();
    FocusScope.of(context).requestFocus(FocusNode());

    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: SafeArea(
            child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/ic_app_background.png"),
                      fit: BoxFit.fill),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: size.height * 0.05),
                      Center(child: AppIconWidget(image: Assets.appLogo)),
                      SizedBox(height: size.height * 0.03),
                      Text(
                        "Sign up to continue",
                        style: TextStyle(
                          fontFamily: 'Alegreya Sans',
                          fontSize: 25,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(38, 47, 113, 0.8),
                        ),
                      ),
                      SizedBox(height: size.height * 0.03),
                      new Stack(
                        children: <Widget>[
                          Container(
                            decoration: new BoxDecoration(color: Colors.white),
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(top: 5.0),
                            child: new Card(
                              elevation: 3.0,
                              semanticContainer: true,
                              margin: EdgeInsets.all(30),
                              child: new Column(
                                children: <Widget>[
                                  SizedBox(height: size.height * 0.05),
                                  Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: new Column(
                                      children: <Widget>[
                                        TextField(
                                          controller: _parentNameController,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  204, 204, 204, 1.0),
                                              fontFamily: 'Alegreya Sans',
                                              fontSize: 18,
                                              fontWeight: FontWeight.w300),
                                          decoration: InputDecoration(
                                            contentPadding:
                                                EdgeInsets.all(10.0),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromRGBO(
                                                      227, 109, 166, 1.0)),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromRGBO(
                                                      227, 109, 166, 1.0)),
                                            ),
                                            hintText: "Parent Name",
                                            suffixIcon: IconButton(
                                                icon: Icon(Icons.person_outline,
                                                    color: Color.fromRGBO(
                                                        227, 109, 166, 1.0))),
                                          ),
                                        ),
                                        SizedBox(height: size.height * 0.01),
                                        TextField(
                                          controller: _mobileNoController,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  204, 204, 204, 1.0),
                                              fontFamily: 'Alegreya Sans',
                                              fontSize: 18,
                                              fontWeight: FontWeight.w300),
                                          decoration: InputDecoration(
                                            contentPadding:
                                                EdgeInsets.all(10.0),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromRGBO(
                                                      227, 109, 166, 1.0)),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromRGBO(
                                                      227, 109, 166, 1.0)),
                                            ),
                                            hintText: "Mobile Number",
                                            suffixIcon: IconButton(
                                                icon: Icon(
                                                    Icons
                                                        .phone_android_outlined,
                                                    color: Color.fromRGBO(
                                                        227, 109, 166, 1.0))),
                                          ),
                                        ),
                                        SizedBox(height: size.height * 0.01),
                                        TextField(
                                          controller: _emailIdController,
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  204, 204, 204, 1.0),
                                              fontFamily: 'Alegreya Sans',
                                              fontSize: 18,
                                              fontWeight: FontWeight.w300),
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.only(
                                                left: 10.0, top: 10, right: 10),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromRGBO(
                                                      227, 109, 166, 1.0)),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromRGBO(
                                                      227, 109, 166, 1.0)),
                                            ),
                                            hintText: "Email id",
                                            suffixIcon: IconButton(
                                                icon: Icon(Icons.email_outlined,
                                                    color: Color.fromRGBO(
                                                        227, 109, 166, 1.0))),
                                          ),
                                        ),
                                        SizedBox(height: size.height * 0.01),
                                        Container(
                                          height: 55.0,
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            
                                          ),
                                          child: new DropdownButton(
                                            value: _value,
                                            iconEnabledColor: Color.fromRGBO(
                                                227, 109, 166, 1.0),
                                            isExpanded: true,

                                            isDense: true,
                                            hint: Text(
                                              "Status",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      204, 204, 204, 1.0),
                                                  fontFamily: 'Alegreya Sans',
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w300),
                                            ),
                                              items: [
                                                DropdownMenuItem(
                                                  child: Text("City"),
                                                  value: 1,
                                                ),
                                                DropdownMenuItem(
                                                  child: Text("Mumbai"),
                                                  value: 2,
                                                ),
                                                DropdownMenuItem(
                                                    child: Text("Pune"),
                                                    value: 3
                                                ),
                                                DropdownMenuItem(
                                                    child: Text("Delhi"),
                                                    value: 4
                                                )
                                              ],
                                              onChanged: (value) {
                                                setState(() {
                                                  _value = value;
                                                });
                                              }),
                                          ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(14.0),
                                            child: Text('Number of Children',
                                                style: TextStyle(fontSize: 15,color: Color.fromRGBO(
                                                    227, 109, 166, 1.0)))
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Radio(
                                              value: 1,
                                              activeColor: Color.fromRGBO(
                                                  227, 109, 166, 1.0),
                                              groupValue: number,
                                              onChanged: (val) {
                                                setState(() {
                                                  radioButtonItem = 'ONE';
                                                  number = 1;
                                                });
                                              },
                                            ),
                                            Text(
                                              'ONE',
                                              style: new TextStyle(fontSize: 15.0),
                                            ),

                                            Radio(
                                              value: 2,
                                              groupValue: number,
                                              onChanged: (val) {
                                                setState(() {
                                                  radioButtonItem = 'TWO';
                                                  number = 2;
                                                });
                                              },
                                            ),
                                            Text(
                                              'TWO',
                                              style: new TextStyle(
                                                fontSize: 15.0,
                                              ),
                                            ),

                                            Radio(
                                              value: 3,
                                              groupValue: number,
                                              onChanged: (val) {
                                                setState(() {
                                                  radioButtonItem = 'THREE';
                                                  number = 3;
                                                });
                                              },
                                            ),
                                            Text(
                                              'THREE',
                                              style: new TextStyle(fontSize: 15.0),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                        Container(
                                          height: 55.0,
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                            color: Colors.white,

                                          ),
                                          child: new DropdownButton(
                                              value: _second_value,
                                              iconEnabledColor: Color.fromRGBO(
                                                  227, 109, 166, 1.0),
                                              isExpanded: true,

                                              isDense: true,
                                              hint: Text(
                                                "Status",
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        204, 204, 204, 1.0),
                                                    fontFamily: 'Alegreya Sans',
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w300),
                                              ),
                                              items: [
                                                DropdownMenuItem(
                                                  child: Text("Relation with child"),
                                                  value: 1,
                                                ),
                                                DropdownMenuItem(
                                                  child: Text("Mother"),
                                                  value: 2,
                                                ),
                                                DropdownMenuItem(
                                                    child: Text("Father"),
                                                    value: 3
                                                ),
                                                DropdownMenuItem(
                                                    child: Text("Guardian"),
                                                    value: 4
                                                )
                                              ],
                                              onChanged: (value) {
                                                setState(() {
                                                  _second_value = value;
                                                });
                                              }),
                                        ),
                                        SizedBox(height: size.height * 0.05),
                                        RaisedButton(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 15, horizontal: 60),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25.0),
                                          ),
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DashboardScreen()));
                                          },
                                          color: Color.fromRGBO(
                                              227, 109, 166, 1.0),
                                          textColor: Colors.white,
                                          child: Text("Next",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: 'Alegreya Sans',
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        SizedBox(height: size.height * 0.02),
                                        new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              "Already Have An Account ? ",
                                              style: TextStyle(
                                                fontFamily: 'NexaDemo',
                                                fontSize: 15,
                                                fontWeight: FontWeight.w300,
                                                color: Color.fromRGBO(
                                                    38, 47, 113, 0.8),
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: (){
                                                navigateToSignIn(context);
                                              },
                                              child: Text(
                                                "Sign In",
                                                style: TextStyle(
                                                  fontFamily: 'NexaDemo',
                                                  fontSize: 15,
                                                  color: Color.fromRGBO(
                                                      38, 47, 113, 0.8),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        SizedBox(height: size.height * 0.01),

                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Image.asset(
                              Assets.pin,
                              width: 90,
                              height: 111,
                            ),
                          )
                        ],
                      ),
                      GestureDetector(
                        child: Text(
                          "Skip",
                          style: TextStyle(
                            fontFamily: 'Alegreya San',
                            fontSize: 15,
                            color: Color.fromRGBO(38, 47, 113, 0.8),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.06),

                    ],
                  ),
                ))));
  }
  navigateToSignIn(context){
    FocusScope.of(context).requestFocus(FocusNode());
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));
  }
}
