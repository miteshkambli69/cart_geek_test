import 'package:cart_geek/screens/dashboard/components/menu/zoom_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'circular_image.dart';

class MenuScreen extends StatelessWidget {
  final String imageUrl =
      "https://celebritypets.net/wp-content/uploads/2016/12/Adriana-Lima.jpg";

  final List<MenuItem> options = [
    MenuItem(Icons.search, 'Home'),
    MenuItem(Icons.shopping_basket, 'Book A Nanny'),
    MenuItem(Icons.favorite, 'How Its Works'),
    MenuItem(Icons.code, 'Why Nanny Vanny'),
    MenuItem(Icons.format_list_bulleted, 'My Bookings'),
    MenuItem(Icons.format_list_bulleted, 'My Profile'),
    MenuItem(Icons.format_list_bulleted, 'Support'),

  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: (details) {
        //on swiping left
        if (details.delta.dx < -6) {
          Provider.of<MenuController>(context, listen: true).toggle();
        }
      },
      child: Container(
        padding: EdgeInsets.only(
            top: 62,
            left: 32,
            bottom: 8,
            right: MediaQuery.of(context).size.width / 2.9),
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Column(
                  children: <Widget>[
                    CircleAvatar(
                        radius: 35,
                        child: Image.asset(
                            "assets/images/ic_profile.png")),
                    SizedBox(height: MediaQuery.of(context).size.height * 0.03),


                    Text(
                      "Emily Cyrus",
                      style: TextStyle(
                        fontFamily: 'Alegreya Sans',
                        fontSize: 20,
                        color: Color.fromRGBO(227, 109, 166, 1.0),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),

              ],
            ),
            Spacer(),
            Column(
              children: options.map((item) {
                return ListTile(

                  title: Text(
                    item.title,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color:Color.fromRGBO(8, 47, 113, 0.8)),
                  ),
                );
              }).toList(),
            ),
            Spacer(),
            Spacer(),

          ],
        ),
      ),
    );
  }
}

class MenuItem {
  String title;
  IconData icon;

  MenuItem(this.icon, this.title);
}