import 'dart:ui';

import 'package:cart_geek/api/api_response.dart';
import 'package:cart_geek/bloc/dashboard_bloc.dart';
import 'package:cart_geek/constants/assets.dart';
import 'package:cart_geek/model/DashboardModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'menu/menu_page.dart';
import 'menu/zoom_scaffold.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Dashboard',
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  DashboardBloc _bloc;
  bool isCollapsed = true;
  bool isLoad = false;
  double screenHeight, screenWidth;
  final Duration duration = const Duration(milliseconds: 300);
  AnimationController _controller;
  Animation<double> _scaleAnimation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = AnimationController(vsync: this, duration: duration);
    _scaleAnimation = Tween<double>(begin: 1, end: 0.6).animate(_controller);
    _bloc = DashboardBloc();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    _bloc.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;
    return Scaffold(
      body: Stack(
        children: <Widget>[dashboard(context)],
      ),
    );
  }

  Widget dashboard(context) {
    return Container(
      child: RefreshIndicator(
        onRefresh: () => _bloc.fetchDashBoardList(),
        child: StreamBuilder<ApiResponse<DashboardModel>>(
          stream: _bloc.dashboardListStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);

                  break;
                case Status.COMPLETED:
                  return Stack(
                    children: [
                      MenuScreen(),
                      AnimatedPositioned(
                        duration: duration,
                        top: 0,
                        bottom: 0,
                        left: isCollapsed ? 0 : 0.6 * screenWidth,
                        right: isCollapsed ? 0 : -0.4 * screenWidth,
                        child: Container(
                          child: ScaleTransition(
                            scale: _scaleAnimation,
                            child: Scaffold(
                                body: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  padding: const EdgeInsets.only(
                                      left: 16, right: 16, top: 40),
                                  child: SingleChildScrollView(
                                    child: Column(
                                      children: <Widget>[
                                        Align(
                                          alignment: Alignment.topRight,
                                          child: IconButton(
                                              icon: Image.asset(
                                                  'assets/images/ic_menu.png'),
                                              iconSize: 40,
                                              onPressed: () {
                                                setState(() {
                                                  if (isCollapsed)
                                                    _controller.forward();
                                                  else
                                                    _controller.reverse();
                                                  isCollapsed = !isCollapsed;
                                                });
                                              }),
                                        ),
                                        SingleChildScrollView(
                                          child: Container(
                                              margin: EdgeInsets.all(10),
                                              child: Column(
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      CircleAvatar(
                                                          radius: 35,
                                                          child: Image.asset(
                                                              "assets/images/ic_profile.png")),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.all(20),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "Welcome",
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                              style: TextStyle(
                                                                fontFamily:
                                                                    'Alegreya Sans',
                                                                fontSize: 15,
                                                                color: Color
                                                                    .fromRGBO(
                                                                        92,
                                                                        92,
                                                                        92,
                                                                        1.0),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                            Text(
                                                              snapshot.data.data
                                                                  .name
                                                                  .toString(),
                                                              style: TextStyle(
                                                                fontFamily:
                                                                    'NexaDemo',
                                                                fontSize: 18,
                                                                color: Color
                                                                    .fromRGBO(
                                                                        227,
                                                                        109,
                                                                        166,
                                                                        1.0),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  new Stack(
                                                    children: <Widget>[
                                                      Container(
                                                        height: 180,
                                                        margin: EdgeInsets.only(
                                                            top: 30),
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .height *
                                                            15,
                                                        child: Card(
                                                            shape:
                                                                RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          15.0),
                                                            ),
                                                            color:
                                                                Color.fromRGBO(
                                                                    227,
                                                                    109,
                                                                    166,
                                                                    1.0),
                                                            semanticContainer:
                                                                true,
                                                            child: Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      top: 70,
                                                                      left: 20),
                                                              child: Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Text(
                                                                    snapshot
                                                                        .data
                                                                        .data
                                                                        .bannerTitle
                                                                        .toString(),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .start,
                                                                    style:
                                                                        TextStyle(
                                                                      fontFamily:
                                                                          'Alegreya Sans',
                                                                      fontSize:
                                                                          16,
                                                                      color: Color.fromRGBO(
                                                                          38,
                                                                          47,
                                                                          113,
                                                                          1.0),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                      height: MediaQuery.of(context)
                                                                              .size
                                                                              .height *
                                                                          0.01),
                                                                  FlatButton(
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                18.0),
                                                                        side: BorderSide(
                                                                            color: Color.fromRGBO(
                                                                                38,
                                                                                47,
                                                                                113,
                                                                                1.0))),
                                                                    color: Color
                                                                        .fromRGBO(
                                                                            38,
                                                                            47,
                                                                            113,
                                                                            1.0),
                                                                    textColor:
                                                                        Colors
                                                                            .white,
                                                                    padding:
                                                                        EdgeInsets.all(
                                                                            5.0),
                                                                    onPressed:
                                                                        () {},
                                                                    child: Text(
                                                                      "Add to Cart"
                                                                          .toUpperCase(),
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            9.0,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            )),
                                                      ),
                                                      Align(
                                                        alignment: Alignment
                                                            .bottomRight,
                                                        child: Image.asset(
                                                            Assets.dashboard,
                                                            width: 410,
                                                            height: 250),
                                                      ),
                                                    ],
                                                  ),
                                                  Container(
                                                    child: Align(
                                                      alignment:
                                                          Alignment.topLeft,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              "Your Current Booking",
                                                              style: TextStyle(
                                                                fontFamily:
                                                                    'Alegreya Sans',
                                                                fontSize: 18,
                                                                color: Color
                                                                    .fromRGBO(
                                                                        38,
                                                                        47,
                                                                        113,
                                                                        1.0),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                              height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height *
                                                                  0.01),
                                                          Container(
                                                            height: 250,
                                                            padding:
                                                                EdgeInsets.all(
                                                                    5),
                                                            child: Card(
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15.0),
                                                              ),
                                                              elevation: 2,
                                                              child: Column(
                                                                children: <
                                                                    Widget>[
                                                                  Padding(
                                                                    padding:
                                                                        EdgeInsets.all(
                                                                            10),
                                                                    child: Row(
                                                                      children: <
                                                                          Widget>[
                                                                        Text(
                                                                          "One Day Package",
                                                                          textAlign:
                                                                              TextAlign.left,
                                                                          style: TextStyle(
                                                                              fontWeight: FontWeight.bold,
                                                                              color: Color.fromRGBO(227, 109, 166, 1.0)),
                                                                        ),
                                                                        Spacer(),
                                                                        FlatButton(
                                                                          shape:
                                                                              RoundedRectangleBorder(
                                                                            borderRadius:
                                                                                BorderRadius.circular(22.0),
                                                                          ),
                                                                          color: Color.fromRGBO(
                                                                              227,
                                                                              109,
                                                                              166,
                                                                              1.0),
                                                                          textColor:
                                                                              Colors.white,
                                                                          onPressed:
                                                                              () {},
                                                                          child:
                                                                              Text(
                                                                            "Start",
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: 10.0,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    child:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .all(
                                                                          10.0),
                                                                      child:
                                                                          Row(
                                                                        children: <
                                                                            Widget>[
                                                                          Expanded(
                                                                            child:
                                                                                Column(
                                                                              // align the text to the left instead of centered
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                              children: <Widget>[
                                                                                Text(
                                                                                  'From',
                                                                                  style: TextStyle(fontSize: 16, fontFamily: 'Alegreya Sans', fontWeight: FontWeight.bold),
                                                                                ),
                                                                                RichText(
                                                                                  text: TextSpan(
                                                                                    children: [
                                                                                      WidgetSpan(
                                                                                        child: Container(
                                                                                          margin: EdgeInsets.all(2.0),
                                                                                          child: Image.asset("assets/images/ic_small_cal.png", height: 15),
                                                                                        ),
                                                                                      ),
                                                                                      TextSpan(
                                                                                        text: snapshot.data.data.currentBookings.from.date,
                                                                                        style: TextStyle(
                                                                                          fontSize: 16,
                                                                                          fontFamily: 'Alegreya Sans',
                                                                                          fontWeight: FontWeight.w500,
                                                                                          color: Color.fromRGBO(73, 73, 73, 1.0),
                                                                                        ),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                RichText(
                                                                                  text: TextSpan(
                                                                                    children: [
                                                                                      WidgetSpan(
                                                                                        child: Container(margin: EdgeInsets.all(2.0), child: Image.asset("assets/images/ic_clock.png", height: 15)),
                                                                                      ),
                                                                                      TextSpan(
                                                                                        text: snapshot.data.data.currentBookings.from.time,
                                                                                        style: TextStyle(fontSize: 16, color: Color.fromRGBO(73, 73, 73, 1.0), fontFamily: 'Alegreya Sans', fontWeight: FontWeight.w500),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Expanded(
                                                                            child:
                                                                                Column(
                                                                              // align the text to the left instead of centered
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                              children: <Widget>[
                                                                                Text(
                                                                                  'To',
                                                                                  style: TextStyle(fontSize: 16, fontFamily: 'Alegreya Sans', fontWeight: FontWeight.bold),
                                                                                ),
                                                                                RichText(
                                                                                  text: TextSpan(
                                                                                    children: [
                                                                                      WidgetSpan(
                                                                                        child: Container(
                                                                                          margin: EdgeInsets.all(2.0),
                                                                                          child: Image.asset("assets/images/ic_small_cal.png", height: 15),
                                                                                        ),
                                                                                      ),
                                                                                      TextSpan(
                                                                                        text: snapshot.data.data.currentBookings.to.date,
                                                                                        style: TextStyle(
                                                                                          fontSize: 16,
                                                                                          fontFamily: 'Alegreya Sans',
                                                                                          fontWeight: FontWeight.w500,
                                                                                          color: Color.fromRGBO(73, 73, 73, 1.0),
                                                                                        ),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                RichText(
                                                                                  text: TextSpan(
                                                                                    children: [
                                                                                      WidgetSpan(
                                                                                        child: Container(margin: EdgeInsets.all(2.0), child: Image.asset("assets/images/ic_clock.png", height: 15)),
                                                                                      ),
                                                                                      TextSpan(
                                                                                        text: snapshot.data.data.currentBookings.to.time,
                                                                                        style: TextStyle(fontSize: 16, color: Color.fromRGBO(73, 73, 73, 1.0), fontFamily: 'Alegreya Sans', fontWeight: FontWeight.w500),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                      height: MediaQuery.of(context)
                                                                              .size
                                                                              .height *
                                                                          0.01),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceEvenly,
                                                                    children: <
                                                                        Widget>[
                                                                      new FlatButton(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius:
                                                                                BorderRadius.circular(18.0),
                                                                            side: BorderSide(color: Color.fromRGBO(38, 47, 113, 1.0))),
                                                                        color: Color.fromRGBO(
                                                                            38,
                                                                            47,
                                                                            113,
                                                                            1.0),
                                                                        textColor:
                                                                            Colors.white,
                                                                        padding:
                                                                            EdgeInsets.all(5.0),
                                                                        onPressed:
                                                                            () {},
                                                                        child:
                                                                            Row(
                                                                          // Replace with a Row for horizontal icon + text
                                                                          children: <
                                                                              Widget>[
                                                                            Image.asset("assets/images/ic_star.png",
                                                                                height: 15),
                                                                            Text("Rate Us")
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      new FlatButton(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius:
                                                                                BorderRadius.circular(18.0),
                                                                            side: BorderSide(color: Color.fromRGBO(38, 47, 113, 1.0))),
                                                                        color: Color.fromRGBO(
                                                                            38,
                                                                            47,
                                                                            113,
                                                                            1.0),
                                                                        textColor:
                                                                            Colors.white,
                                                                        padding:
                                                                            EdgeInsets.all(5.0),
                                                                        onPressed:
                                                                            () {},
                                                                        child:
                                                                            Row(
                                                                          // Replace with a Row for horizontal icon + text
                                                                          children: <
                                                                              Widget>[
                                                                            Image.asset("assets/images/ic_location.png",
                                                                                height: 15),
                                                                            Text("Geolocation")
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      new FlatButton(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius:
                                                                                BorderRadius.circular(18.0),
                                                                            side: BorderSide(color: Color.fromRGBO(38, 47, 113, 1.0))),
                                                                        color: Color.fromRGBO(
                                                                            38,
                                                                            47,
                                                                            113,
                                                                            1.0),
                                                                        textColor:
                                                                            Colors.white,
                                                                        padding:
                                                                            EdgeInsets.all(5.0),
                                                                        onPressed:
                                                                            () {},
                                                                        child:
                                                                            Row(
                                                                          // Replace with a Row for horizontal icon + text
                                                                          children: <
                                                                              Widget>[
                                                                            Image.asset("assets/images/ic_survellance.png",
                                                                                height: 15),
                                                                            Text("Survillence")
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10,
                                                                    top: 20),
                                                            child: Text(
                                                              "Packages",
                                                              style: TextStyle(
                                                                fontFamily:
                                                                    'Alegreya Sans',
                                                                fontSize: 18,
                                                                color: Color
                                                                    .fromRGBO(
                                                                        38,
                                                                        47,
                                                                        113,
                                                                        1.0),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                          ),
                                                          new ListView.builder(
                                                            shrinkWrap: true,
                                                            physics:
                                                                const NeverScrollableScrollPhysics(),
                                                            itemCount: snapshot
                                                                        .data
                                                                        .data
                                                                        .allPackages ==
                                                                    null
                                                                ? 0
                                                                : snapshot
                                                                    .data
                                                                    .data
                                                                    .allPackages
                                                                    .length,
                                                            itemBuilder:
                                                                (BuildContext
                                                                        context,
                                                                    int index) {
                                                              return new Card(
                                                                elevation: 0.0,
                                                                child:
                                                                    new Container(
                                                                  height: 200,
                                                                  child: Card(
                                                                      elevation:
                                                                          0.0,
                                                                      shape:
                                                                          RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(20.0),
                                                                      ),
                                                                      color: (index & 0x01) !=
                                                                              0
                                                                          ? Color.fromRGBO(
                                                                              128,
                                                                              171,
                                                                              219,
                                                                              1.0)
                                                                          : Color.fromRGBO(
                                                                              240,
                                                                              179,
                                                                              205,
                                                                              1.0),
                                                                      child:
                                                                          Padding(
                                                                        padding: EdgeInsets.only(
                                                                            top:
                                                                                10,
                                                                            left:
                                                                                20,
                                                                            right:
                                                                                20),
                                                                        child:
                                                                            Column(
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.start,
                                                                          children: [
                                                                            new Row(
                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                              children: <Widget>[
                                                                                ImageIcon(
                                                                                  AssetImage("assets/images/ic_calender.png"),
                                                                                  color: Color.fromRGBO(227, 109, 166, 1.0),
                                                                                ),
                                                                                FlatButton(
                                                                                  shape: RoundedRectangleBorder(
                                                                                    borderRadius: BorderRadius.circular(18.0),
                                                                                  ),
                                                                                  color: Color.fromRGBO(227, 109, 166, 1.0),
                                                                                  textColor: Colors.white,
                                                                                  padding: EdgeInsets.all(5.0),
                                                                                  onPressed: () {},
                                                                                  child: Text(
                                                                                    "Book Now".toUpperCase(),
                                                                                    style: TextStyle(
                                                                                      fontSize: 9.0,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                                                            new Row(
                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                              children: <Widget>[
                                                                                Text(
                                                                                  snapshot.data.data.allPackages[index].packageName,
                                                                                  textAlign: TextAlign.start,
                                                                                  style: TextStyle(
                                                                                    fontFamily: 'Alegreya Sans',
                                                                                    fontSize: 16,
                                                                                    color: Color.fromRGBO(38, 47, 113, 1.0),
                                                                                    fontWeight: FontWeight.bold,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  snapshot.data.data.allPackages[index].packagePrice,
                                                                                  textAlign: TextAlign.start,
                                                                                  style: TextStyle(
                                                                                    fontFamily: 'Alegreya Sans',
                                                                                    fontSize: 16,
                                                                                    color: Color.fromRGBO(38, 47, 113, 1.0),
                                                                                    fontWeight: FontWeight.bold,
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                                                            Text(
                                                                              snapshot.data.data.allPackages[index].packageDescription,
                                                                              textAlign: TextAlign.start,
                                                                              style: TextStyle(
                                                                                fontFamily: 'Alegreya Sans',
                                                                                fontSize: 12,
                                                                                color: Color.fromRGBO(73, 73, 73, 1.0),
                                                                                fontWeight: FontWeight.normal,
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      )),
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              )),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                bottomNavigationBar: bottomNavigationBar()),
                          ),
                        ),
                      ),
                    ],
                  );
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => _bloc.fetchDashBoardList(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }

  Widget bottomNavigationBar() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.amber,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              offset: Offset(-3, 10),
              spreadRadius: 5,
              blurRadius: 7),
        ],
      ),
      child: BottomNavigationBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        type: BottomNavigationBarType.fixed,
        currentIndex: 0,
        items: [
          BottomNavigationBarItem(
            title: Text(
              'Home',
              style: TextStyle(color: Colors.pinkAccent),
            ),
            icon: IconButton(
                icon: Icon(Icons.home, color: Colors.grey),
                onPressed: () {
                  setState(() {
                    _controller.reverse();
                    isCollapsed = true;
                  });
                }),
          ),
          BottomNavigationBarItem(
              title: Text('Packages'),
              icon: Image.asset(
                'assets/images/ic_sale.png',
                height: 25,
              )),
          BottomNavigationBarItem(
              title: Text('Bookings'),
              icon: Icon(Icons.access_time, color: Colors.grey)),
          BottomNavigationBarItem(
              title: Text('Profile'),
              icon: Icon(Icons.person, color: Colors.grey)),
        ],
      ),
    );
  }
}

class Error extends StatelessWidget {
  final String errorMessage;

  final Function onRetryPressed;

  const Error({Key key, this.errorMessage, this.onRetryPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            errorMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.lightGreen,
              fontSize: 18,
            ),
          ),
          SizedBox(height: 8),
          RaisedButton(
            color: Colors.lightGreen,
            child: Text('Retry', style: TextStyle(color: Colors.white)),
            onPressed: onRetryPressed,
          )
        ],
      ),
    );
  }
}

//
class Loading extends StatelessWidget {
  final String loadingMessage;

  const Loading({Key key, this.loadingMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.pinkAccent,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.pinkAccent),
          ),
        ],
      ),
    );
  }
}
