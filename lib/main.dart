import 'dart:async';

import 'file:///C:/Users/mitesh/AndroidStudioProjects/cart_geek/lib/screens/splash/splash.dart';
import 'package:flutter/material.dart';
import 'file:///C:/Users/mitesh/AndroidStudioProjects/cart_geek/lib/screens/splash/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Auth',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
      home: SplashScreen(),
    );
  }
}